
* Swagger for local environment is available [here](http://localhost:8080/kypo-adaptive-training-rest/api/v1/swagger-ui.html#/)
* H2 in-memory DB can be accessed [here](http://localhost:8080/kypo-adaptive-training-rest/api/v1/h2-console)
