package cz.muni.ics.kypo.training.adaptive.enums;

/**
 * States represented in Training Definition entity.
 */
public enum TDState {

    /**
     * Privated definition state.
     */
    PRIVATED,
    /**
     * Released definition state.
     */
    RELEASED,
    /**
     * Archived definition state.
     */
    ARCHIVED,
    /**
     * Unreleased definition state.
     */
    UNRELEASED
}
